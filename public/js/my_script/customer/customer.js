tbl_customer_show = $('#tbl_customer_show').DataTable({


    "language": {
        "url": "/plugins/datatables/Spanish.json"
    },
    responsive : true,

    ajax: url_get_customer,
    dom: 'Bfrtip',
    buttons: [
        'excel', 'pdf'
    ],
    columns:
        [
            {data: 'nombres', name: 'nombres'},
            {data: 'apellidos', name: 'apellidos'},
            {data: 'identificacion', name: 'identificacion'},
            {data: 'edad', name: 'edad'},
            {data: 'ciudad_nacimiento', name: 'ciudad_nacimiento'},
            {data: 'tipo_contacto', name: 'tipo_contacto'},
            {data: 'tipo_identificacion', name: 'tipo_identificacion'},
            {data: 'alerta', name: 'alerta'},
            {data: 'fecha_creacion', name: 'fecha_creacion'},
            {data: 'id', name: 'id'}
        ],

    columnDefs: [{

        'targets': -1,
        'searchable': false,
        'orderable': false,
        'className': 'dt-body-center',
        'render': function (date, type, full, meta){
            return '<button onclick="edit('+full['id']+ ');" type="button" class="" name="id"  value="' +full['id']+ '">Editar</button>';

        }
    }]
});


function edit(id) {
    $.ajax({
        url : '/clientes/'+id+'/edit',
        dataType: 'JSON',
        type : 'Get'
    }).done(function (r) {
        console.log(r);
        if (r.result == 200 ){
            $('#name').val(r.data[0].nombres);
            $('#idenfiticacion_id').val(r.data[0].identificacion);
            $('#customer_id').val(r.data[0].id);
            $('#modal-edit').modal('show');

        }else if(r.result==404){
            noty_alert('Noticia','Registro no encontrado','warning')
        }

    }).fail(function (r) {
        console.log(r);
    })
}

$('#add_customer').click(function () {
    $('#title').text('Cargar clientes')
    $('#div_email').remove();
    $('#div_coment').remove();
    $('#modal-warning').modal('show');
});
$('#send_file').click(function () {
    $('#title').text('Enviar archivo')
    
    $('#div_file').prepend(`
                    <div class="col-md-12" id="div_email">
                        <div class="form-group">
                            <label for="name">Correo</label>
                            <input id="name" required min="0" max="50" type="email" class="form-control" name="email" value="" autofocus>
                        </div>
                    </div>
                    <div class="col-md-12" id="div_coment">
                        <div class="form-group">
                              <label for="comment">Comentario</label>
                             <textarea class="form-control" name="content" rows="5" id="content_id"></textarea>
                        </div>
                    </div>
`);
    $('#frm_modal').attr('action','/clientes/send/email');
    $('#modal-warning').modal('show');
});

$('#btn_update').click(function () {
    var tipo_contacto = $('#tipo_contacto_id').val();
    var identificacion =  $('#idenfiticacion_id').val();
    var nombres = $('#name').val();
    if (tipo_contacto >0  && nombres.length<=50 && identificacion.length<=15){

    $.ajax({
        url : '/clientes/actualizar',
        dataType : 'JSON',
        type: 'POST',
        data: {
            'nombres' :  nombres,
            'identificacion_id' : identificacion,
            'tipo_contacto' : tipo_contacto,
            'cliente_id': $('#customer_id').val()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function (r) {
        if (r.result == 201){
            tbl_customer_show.ajax.reload();
            noty_alert('Noticia','Registro actualizado correctamente','success');
            $('#modal-edit').modal('hide');
        }else{
            noty_alert('Noticia','Error en la transacción, intente de nuevo','warning');
        }
    }).fail(function (r) {

    });
    }else{
        noty_alert('Noticia','Por favor seleccione un tipo de contacto y valide que este ingresando solo numeros en el campo identificación y que el nombre no exceda 50 caracteres','warning',4000);
    }
});