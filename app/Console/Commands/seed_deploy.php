<?php

namespace App\Console\Commands;

use App\Models\Alerta;
use Illuminate\Console\Command;

class seed_deploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:seed_deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carga inicial en la bd';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (count(Alerta::all())<=0){
            $alert = new \SeedAlerta();
            $alert->run();
            $type_concta = new \SeedTipoContacto();
            $type_concta->run();
            $type_identification = new \SeedTipoIdentificacion();
            $type_identification->run();
            $users = new \Seedusers();
            $users->run();
            $ciudad = new \SeedCiudad();
            $ciudad->run();
        }
    }
}
