<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 15 May 2018 12:28:09 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TipoContacto
 * 
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $clientes
 *
 * @package App\Models
 */
class TipoContacto extends Eloquent
{
	protected $table = 'tipo_contacto';

	protected $fillable = [
		'nombre',
		'codigo'
	];

	public function clientes()
	{
		return $this->hasMany(\App\Models\Cliente::class);
	}
}
