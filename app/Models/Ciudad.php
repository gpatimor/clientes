<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 15 May 2018 12:28:09 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Ciudad
 * 
 * @property int $id
 * @property string $nombre
 * 
 * @property \Illuminate\Database\Eloquent\Collection $clientes
 *
 * @package App\Models
 */
class Ciudad extends Eloquent
{
	protected $table = 'ciudad';
	public $timestamps = false;

	protected $fillable = [
		'nombre'
	];

	public function clientes()
	{
		return $this->hasMany(\App\Models\Cliente::class, 'ciudad_nacimiento_id');
	}
}
