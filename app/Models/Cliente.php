<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 15 May 2018 12:28:09 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cliente
 *
 * @property int $id
 * @property string $nombres
 * @property string $apellidos
 * @property string $identificacion
 * @property int $edad
 * @property int $tipo_identificacion_id
 * @property int $tipo_contacto_id
 * @property int $alerta_id
 * @property int $ciudad_nacimiento_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Alertum $alertum
 * @property \App\Models\Ciudad $ciudad
 * @property \App\Models\TipoContacto $tipo_contacto
 * @property \App\Models\TipoIdentificacion $tipo_identificacion
 *
 * @package App\Models
 */
class Cliente extends Eloquent
{
	protected $table = 'cliente';

	protected $casts = [
		'edad' => 'int',
		'tipo_identificacion_id' => 'int',
		'tipo_contacto_id' => 'int',
		'alerta_id' => 'int',
		'ciudad_nacimiento_id' => 'int'
	];

	protected $fillable = [
		'nombres',
		'apellidos',
		'identificacion',
		'edad',
		'tipo_identificacion_id',
		'tipo_contacto_id',
		'alerta_id',
		'ciudad_nacimiento_id',
        'fecha_creacion'
	];

	public function alerta()
	{
		return $this->belongsTo(\App\Models\Alertum::class, 'alerta_id');
	}

	public function ciudad()
	{
		return $this->belongsTo(\App\Models\Ciudad::class, 'ciudad_nacimiento_id');
	}

	public function tipo_contacto()
	{
		return $this->belongsTo(\App\Models\TipoContacto::class);
	}

	public function tipo_identificacion()
	{
		return $this->belongsTo(\App\Models\TipoIdentificacion::class);
	}
}
