<?php

namespace App\Http\Controllers;

use App\Http\common\CustomerFunctions;
use App\Models\Cliente;
use App\Models\TipoContacto;
use Illuminate\Http\Request;
use Jleon\LaravelPnotify\Notify;


class CustomerController extends Controller
{
    public function send_email(Request $request){

        $input = $request->all();
        if($request->hasFile('file') && $input["email"] ){
            try{
                $file = $request->file('file');
                $name = $file->getClientOriginalName();
                \Storage::disk('local')->put($name,  \File::get($file));
                $pathToFile= storage_path('app') ."/". $name;
                $to_send = $input["email"];
                $sujects = 'Archivo de clientes';
                $data = array('content' => $input['content']);
                \Mail::send('customer.send_email', $data, function ($message) use ($name,$sujects,$to_send, $pathToFile) {
                $message->from('ductuscorporation@gmail.com', 'ductus2017');
                $message->to($to_send)->subject($sujects);
                $message->attach($pathToFile);


                });
                    \Storage::disk('local')->delete($name);
                    Notify::success('Archivo enviado con éxito','Noticia');
                    return redirect('/clientes/show');
            } catch (\Exception $e){
                Notify::error('Error al enviar el adjunto, intente de nuevo','Noticia');
                return redirect('/clientes/show');
            }
        }else{
            Notify::warning('Por favor selecciones un archivo e ingrese un email valido','Noticia');
            return redirect('/clientes/show');

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type_contact = TipoContacto::all();
        return view('customer.show',compact('type_contact',$type_contact));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)    {

        if ($request->hasFile('file')) {
            $instance = new CustomerFunctions();
            $upload_file = $instance->upload_file(fopen($request->file('file')->getRealPath(),'r'));
            if ($upload_file){
                Notify::success('Carga éxitosa','Noticia');
                return redirect('/clientes/show');
            }else{
                Notify::error('Se presentaron algunos errores en la carga, por favor valide e intente de nuevo','Noticia');
                return redirect('/clientes/show');
            }
        } else {
            Notify::info('Por favor seleccione un archivo','Noticia');
            return redirect('/clientes/show');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   $type_contact = TipoContacto::all();
        return view('customer.show',compact('type_contact',$type_contact));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Cliente::select('cliente.id','nombres','identificacion','nombre as tipo_contacto','tipo_contacto_id')
            ->join('tipo_contacto','tipo_contacto.id','=','cliente.tipo_contacto_id')
            ->where('cliente.id',$id)->get();
        if(count($data)>0){
            $result = 200;
        }else{
            $result = 404;
        }
        return json_encode(['result'=>$result,'data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   $input = $request->all();
        $customer = Cliente::find($input['cliente_id']);
        if(count($customer)>0){
            try{
                $type_contact = TipoContacto::find($input['tipo_contacto']);
                $customer->update(['nombres'=>$input['nombres'],'identificacion'=>$input['identificacion_id'],
                    'tipo_contacto_id'=>$type_contact['id']]);
                $result = 201;
            }catch (\Exception $e){
              $result =$e->getMessage();
            }

        }
        return json_encode(['result'=>$result]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function customer_get()
    {
        $instance = new CustomerFunctions();
        $data = $instance->data_customer_all();
        return datatables()
            ->of($data)
            ->toJson();
    }
}
