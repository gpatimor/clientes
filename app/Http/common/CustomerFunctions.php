<?php

namespace App\Http\common;

use App\Models\Alerta;
use App\Models\Ciudad;
use App\Models\Cliente;
use App\Models\TipoContacto;
use App\Models\TipoIdentificacion;

class CustomerFunctions {

    public function data_customer_all(){

        $data = Cliente::select('nombres','apellidos','identificacion','edad','ciudad.nombre as ciudad_nacimiento',
        'tipo_contacto.codigo as tipo_contacto','tipo_identificacion.codigo as tipo_identificacion','alerta.codigo as alerta',
         'fecha_creacion','cliente.id as id'
            )
            ->join('alerta','alerta.id','=','cliente.alerta_id')
            ->join('tipo_identificacion','tipo_identificacion.id','=','cliente.tipo_identificacion_id')
            ->join('ciudad','ciudad.id','=','cliente.ciudad_nacimiento_id')
            ->join('tipo_contacto','tipo_contacto.id','=','cliente.tipo_contacto_id')
            ->get();
        return $data;
    }

    public function upload_file($attached){

        try {



            $auxi = array();
            while($linea = fgetcsv($attached, 0, ";")){
                array_push($auxi,$linea);
                unset($auxi[0]);
            }
            fclose($attached);
            foreach ($auxi as $value){
                \DB::beginTransaction();
                $get_alert = Alerta::select('id')->where('nombre',$value[5])->get();
                $get_identification_type = TipoIdentificacion::select('id')->where('nombre',$value[4])->get();
                $get_contact_type = TipoContacto::select('id')->where('nombre',$value[2])->get();
                $get_birth_city = Ciudad::select('id')->where('nombre',$birth_city = $value[8])->get();
                //dd($get_alert,$get_identification_type,$get_contact_type,$get_birth_city);
                Cliente::create(['nombres'=>$first_name = $value[0],'apellidos'=> $last_name = $value[1],
                    'identificacion'=>$identification = $value[3],'edad'=>$age = $value[6],'created_at'=>$date_create = $value[7],
                    'alerta_id'=>$get_alert[0]['id'],'tipo_identificacion_id'=>$get_identification_type[0]['id'],
                    'ciudad_nacimiento_id'=>$get_birth_city[0]['id'],'tipo_contacto_id'=>$get_contact_type[0]['id'],
                    'fecha_creacion'=>$value[7]]);
                \DB::commit();
            }
            return true;
        } catch (\Exception $e) {
            \DB::RollBack();
            return false;
        }
    }
}