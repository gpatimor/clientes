<div class="modal fade" id="modal-edit" style="display: none; padding-right: 15px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="" action="/clientes" id="frm_load_customer" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Editar clientes</h4>
                </div>
                <input type="hidden" id="customer_id" value="">
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nombres</label>
                            <input id="name" required min="0" max="50" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Identificación</label>
                            <input type ="number" min="0" max="15" id="idenfiticacion_id" type="text" class="form-control" name="identificacion">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="" for="">Tipo contacto</label>
                            <select class="form-control" name="tipo_contacto" id="tipo_contacto_id">
                                <option value="0" selected="">Seleccione...</option>
                                @foreach ($type_contact as $value)
                                    <option value="{{$value->id}}">{{$value->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btn_update"  class="btn btn-primary">Actualizar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>