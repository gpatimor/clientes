<div class="modal fade" id="modal-warning" style="display: none; padding-right: 15px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="frm_modal" class="" action="/clientes" id="frm_load_customer" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 id="title" class="modal-title"></h4>
                </div>
                <div class="modal-body">

                    <div class="form-group" id="div_file">
                        <label for="exampleInputFile">Archivo adjunto</label>
                        <input type="file" name="file" required class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="btn_load"  class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>