<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Clientes</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/css/skins/_all-skins.min.css">


    <!-- DataTable -->
    <link rel="stylesheet" href="/plugins/datatables/datatables.min.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">

    <!-- PNotify -->
    <link rel="stylesheet" href="/plugins/pnotify/pnotify.custom.min.css">

    <!-- select 2-->
    <link rel="stylesheet" href="/plugins/select2-4.0.3/css/select2.css">

    <!-- datetimepicker 3-->
    <link rel="stylesheet" href="/plugins/bootstrapdatepicker/css/bootstrap-datepicker3.min.css">

    <link rel="stylesheet" href="/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css">


</head>

