<!DOCTYPE html>
<html lang="es">
@include('layouts.head')
<body class="skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->

            <a href="{{ route('home') }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
            {{-- <span class="logo-mini"><b>A</b>LT</span> --}}
            <!-- logo for regular state and mobile devices -->
                {{-- <img src="img/logo.png" class="img-responsive center-block" alt=""> --}}
                <span class="logo-lg"><b>Clientes</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->

                        <!-- Tasks: style can be found in dropdown.less -->

                        <!-- User Account: style can be found in dropdown.less -->
                        @if (Auth::check())
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                        <span class="hidden-xs"><b>Hola: </b>{{Auth::user()->name}}</span>


                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->

                                    <!-- Menu Body -->

                                    <!-- Menu Footer-->
                                    <li class="user-footer">

                                        <div class="pull-right">
                                            @if (Auth::check())
                                                <a href="#"
                                                   onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"
                                                   class ="btn btn-default btn-flat">
                                                    Cerrar sesión
                                                </a>
                                                <a href="{{route('home')}}"
                                                   class ="btn btn-default btn-flat">
                                                    Menú
                                                </a>

                                            @else

                                            @endif
                                            @endif
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                    </ul>
                </div>
            </nav>
        </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    {{-- <img src="img/logo.png" class="img-responsive center-block" alt=""> --}}
                </div>

            </div>
            <!-- search form -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->

            @if (Auth::check())

            <ul class="sidebar-menu">
                @include('layouts.navbar')

                </ul>
            @endif

        </section>

        <!-- /.sidebar -->
    </aside>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('title')
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            @yield('content')
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> prueba
        </div>
        <strong>Copyright &copy; 2018-Clientes:</strong> Todos los derechos reservados
    </footer>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>

    <!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script
            src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- dataTables -->
    <script src="/plugins/datatables/datatables.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js" ></script>
    <!-- SlimScroll -->
    <script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/plugins/fastclick/fastclick.js"></script>



    {{-- <script src="dist/js/app.js"></script> --}}
    <!-- AdminLTE for demo purposes -->
    <script src="/js/app.min.js"></script>
    <script src="/js/demo.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->





    <!-- PNotify -->
    <script src="/plugins/pnotify/pnotify.custom.min.js"></script>

    <!-- Select2 -->
    <script src="/plugins/select2-4.0.3/js/select2.min.js"></script>


    <!-- datepicker -->
    <script src="/plugins/bootstrapdatepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/plugins/bootstrapdatepicker/locales/bootstrap-datepicker.es.min.js"></script>

    <script src="/plugins/chartjs/Chart.min.js"></script>
    <script src="/js/my_script/script.js"></script>

    <![endif]-->


    @if (Session::has('notifier.notice'))
        <script>
            window.onload=function(){
                Objeto=document.getElementsByTagName("a");
                for(a=0;a<Objeto.length;a++){
                    Objeto[a].onclick=function(){
                        location.replace(this.href);
                        return false;
                    }
                }
            }
            new PNotify({!! Session::get('notifier.notice') !!});
        </script>
    @endif


@yield('script')
</body>
</html>
