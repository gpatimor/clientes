<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>correo</title>
    <style>

        .titulo {
            color: #1e80b6;
            padding-top: 20px;
            padding-bottom: 10px;
            padding-left: 20px;
            padding-right: 20px;
        }

        .body{
            background-color: #ECECEC;
        }

        .div_contenido{
            color: #1e80b6;
            padding-top: 20px;
            padding-bottom: 10px;
            padding-left: 20px;
            padding-right: 20px;
            background-color: #ffffff !important;
        }

    </style>

</head>

<body class="body">

<div class="titulo" > <h1>Información Clientes</h1></div>
<hr>
<div class=".div_contenido" > <?= $content;   ?></div>
<hr>
<br>
<div class=".div_contenido" > Gracias por usar la aplicación <b>Customers</b></div>

</body>
</html>