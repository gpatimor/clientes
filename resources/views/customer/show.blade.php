@extends('layouts.app')

@section('title')
    Lista de clientes
    @endsection()

@section('content')
    <div id ="frm_customer_show" class="box" id="Document">
        <div class="box-header">
        </div>
        <div class="box-body">
            <table class="table" id="tbl_customer_show">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>idenfificación</th>
                        <th>Edad</th>
                        <th>Ciudad nacimiento</th>
                        <th>Tipo contacto</th>
                        <th>Tipo indenficación</th>
                        <th>Alerta</th>
                        <th>Fecha creación</th>
                        <th>Opción</th>
                    </tr>
                </thead>
            </table>
            <div class="col-12 box-footer">
            <button data-toggle="tooltip" title="Cargar clientes nuevos" id="add_customer" class="btn btn-success btn-md pull-right">Cargar clientes</button>
            <button data-toggle="tooltip" title="Enviar archivo generado por email" id="send_file" class="btn btn-info btn-md pull-right">Enviar archivo</button>
            </div>
        </div>
    </div>
    <div class="row">
        @component(
        'partials.customer_modal'        
        )
        @endcomponent
    </div>
    <div class="row">
        @component(
        'partials.customer_modal_edit',
        ["type_contact"=>$type_contact]
        )
        @endcomponent
    </div>
@endsection()

@section('script')
    <script>
        url_get_customer = '{{ route('customer_get') }}'
    </script>
    <script src="{{ asset('/js/my_script/customer/customer.js')}}"></script>
@endsection()
