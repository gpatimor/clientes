<?php

use Illuminate\Database\Seeder;

class SeedTipoContacto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_contacto')->insert(['nombre' => 'RL','codigo' => 'RL']);
        DB::table('tipo_contacto')->insert(['nombre' => 'N/A','codigo' => 'N/A']);
        DB::table('tipo_contacto')->insert(['nombre' => 'SC','codigo' => 'SC']);

    }
}
