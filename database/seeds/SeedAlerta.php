<?php

use Illuminate\Database\Seeder;

class SeedAlerta extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alerta')->insert(['nombre' => 'SI','codigo' => 'SI']);
        DB::table('alerta')->insert(['nombre' => 'INCONSISTENCIA','codigo' => 'INC']);
    }
}
