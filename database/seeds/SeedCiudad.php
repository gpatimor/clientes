<?php

use Illuminate\Database\Seeder;

class SeedCiudad extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ciudad')->insert(['nombre' => 'Medellin']);
        DB::table('ciudad')->insert(['nombre' => 'Itagui']);
        DB::table('ciudad')->insert(['nombre' => 'Bello']);

    }
}
