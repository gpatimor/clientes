<?php

use Illuminate\Database\Seeder;

class Seedusers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([            [
            'name' => 'Administrador',
            'email' => 'ductuscorporation@gmail.com',
            'password' => bcrypt('admin 123'),
            'remember_token' => null
        ]]);
    }
}
