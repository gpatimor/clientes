<?php

use Illuminate\Database\Seeder;

class SeedTipoIdentificacion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_identificacion')->insert(['nombre' => 'NIT','codigo' => 'NIT']);
        DB::table('tipo_identificacion')->insert(['nombre' => 'CC','codigo' => 'CC']);
    }
}
