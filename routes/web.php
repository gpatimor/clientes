<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'CustomerController@index')->name('home');

/*
* Routes for customers
 */
 Route::get('/clientes/get','CustomerController@customer_get')->name('customer_get');
 Route::POST('/clientes/actualizar','CustomerController@update')->name('customer_update');
 Route::POST('/clientes/send/email','CustomerController@send_email')->name('customer_send_email');
 Route::resource('/clientes','CustomerController');
